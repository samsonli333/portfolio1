import React from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap';
import { State } from '../App';
import ModalSwitch from './ModalSwitch'
import { whatsappIcon } from '../photo/photo'
import { whatsapp } from '../function/function'


class BottomNav extends React.Component {
    constructor(props: {}) {
        super(props)
    }

    public render() {
        return (
            <ModalSwitch.Consumer>
                {({ switchOn }: State) => (
                    <>
                        <Navbar bg="light" className="bottom-navbar">
                            <Container fluid className="Navbar-Width mx-auto">
                                <Nav className="mx-auto bottom-nav">
                                    <div className="nav-item-top">
                                        <Nav.Link href="#action1" className="liveChat" onClick={() => whatsapp.genericCall()}><img alt="#" className="liveChat-img" src={whatsappIcon}></img><div className="liveChat-text">Live Chat</div></Nav.Link>
                                        <Nav.Link href="#action2" className="subscribe-top-button" onClick={() => switchOn()}><div className="subscrible-top-text">Subscribe Now!</div></Nav.Link>
                                    </div>
                                    <div className="nav-item-bottom"><Nav.Link href="#action3" className="otherBenefit"><div className="otherBenefit-text">+ 免6個月費用 及 安裝費</div></Nav.Link>
                                    </div>
                                </Nav>
                            </Container>
                        </Navbar></>
                )}
            </ModalSwitch.Consumer>
        )
    }
}

export default BottomNav;