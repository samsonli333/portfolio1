import React from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap';
import { State } from '../App';
import { Logo, whatsappIcon } from '../photo/photo'
import ModalSwitch from './ModalSwitch'
import { whatsapp } from '../function/function'

class TopNav extends React.Component {
    constructor(props: {}) {
        super(props)
    }

    public render() {
        return (
            <ModalSwitch.Consumer>
                {({ switchOn }: State) => (
                    <>
                        <Navbar bg="white" expand="lg">
                            <Container fluid className="Navbar-Width">
                                <Navbar.Brand href="#"><img alt="#" className="Logo-img" src={Logo}></img></Navbar.Brand>
                                <Navbar.Collapse id="navbarScroll">
                                    <Nav
                                        className="ms-auto my-2 my-lg-0"
                                        style={{ maxHeight: '100px' }}
                                        navbarScroll
                                    >
                                        <Nav.Link href="#" className="liveChat" onClick={() => whatsapp.genericCall()}><img alt="#" className="liveChat-img" src={whatsappIcon}></img><div className="liveChat-text">Live Chat</div></Nav.Link>
                                        <Nav.Link href="#" className="subscribe-top-button" onClick={() => switchOn()}><div className="subscrible-top-text">Subscribe Now!</div></Nav.Link>
                                        <Nav.Link href="#" className="otherBenefit"><div className="otherBenefit-text">+ 免6個月費用 及 安裝費</div></Nav.Link>
                                    </Nav>
                                </Navbar.Collapse>
                            </Container>
                        </Navbar></>
                )}
            </ModalSwitch.Consumer>
        )
    }
}

export default TopNav;