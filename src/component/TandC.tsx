import React from 'react'
import { Collapse } from 'react-bootstrap';
import { arrowTandC } from '../photo/photo'
import {arrow} from '../function/function'


interface State {
  open: boolean;
}

class TandC extends React.Component<{}, State>{
  public state: State
  constructor(props: {}) {
    super(props)
    this.state = {
      open: false
    }
  }

public arrowUp():void{
  const Arrow:arrow = new arrow()
  Arrow.up()
}

public arrowDown():void{
  const Arrow:arrow = new arrow()
  Arrow.down()
}

  public render() {

    return (
      <>
        <div className="t-and-c">
          <div className="accordion-header"
            onClick={() => this.setState(prevState => ({ open: !prevState.open }))}
            aria-controls="example-collapse-text"
          >
            <div className = "t-and-c-text">條款及細則</div>
            <img className="arrow" alt="#" src={arrowTandC}></img>
          </div>
          <Collapse in={this.state.open} onEnter={() => this.arrowUp()} onExit={() => this.arrowDown()}>
            <div id="example-collapse-text" className="accordion-body">
              <br/><br/>
              Broadband Anti Inflation Promotion - Terms and conditions
              優惠期有限，額滿即止。此優惠只適用於指定屋苑及安裝地址須於登記前60日內未曾使用有線電視/寬頻的任何服務。
              有線1000M光纖寬頻+Tenda AC11 無線雙頻Gigabit路由器優惠月費$88起。有線200M家居寬頻+Tenda AC11 無線雙頻Gigabit路由器優惠月費$68起。須連續訂購36個月。公私屋苑劃一價。有線1000M光纖寬頻上網服務最高上/下傳速度分別為1000Mbps。有線200M寬頻上網服務最高下傳速度為200Mbps共享、上傳速度為10Mbps共享。所有服務之頻寬速度指由用戶單位之牆身插座至有線寬頻第一台網絡器材之連線規格。實際速度會因使用者的設備、傳送技術、個別網絡及軟件之使用、網絡設定、覆蓋範圍、使用量及其他外在因素而有所影響。成功安裝有線寬頻之新登記用戶可享：(1)豁免安裝費 (價值$600)、(2)首次搬遷費(價值$350)，額滿即止。如用戶於服務承諾使用期期間終止服務，須繳付提前終止服務費用(等於服務承諾使用期餘下月份之所有服務費用)。有線寬頻並保留追討或在用戶終止服務前於其賬戶扣除相等於一切已送出之Tenda AC11 無線雙頻Gigabit路由器(「無線路由器」)(價值港幣$338)或功能規格類近之產品同等價值的款項的權利。已繳付之所有費用概不退還，亦不可轉讓他人。無線路由器及無線系統均豁免安裝費及送貨費用。無線路由器的保養服務直接由「迅靈科技有限公司」提供36個月保養服務。有線寬頻及其員工或銷售代理只負責提供無線路由器予有線寬頻用戶。任何有關裝置及配件之維修、保養、更換或投訴，用戶必須直接聯絡製造商或代理商，並支付有關費用（如適用），聯絡方法請參閱包裝盒上的說明。有線寬頻之無線路由器及無線系統為支持 IEEE 802.11ac制式，其實際輸出值及詳細規格，用戶請參閱產品包裝盒上說明。有線寬頻概不負責無線路由器之產品功能或實際表現的相關問題，用戶必須直接聯絡製造商或代理商查詢了解。請自行保留無線路由器及無線系統的送貨單，維修時須攜同保用證及送貨單方可享用保養服務。保用期由送貨日期起計算。優惠受有關條款及細則約束。如有任何爭議，有線電視將保留最終決定權。服務由香港有線電視有限公司提供。現有用戶可致電優惠專線183 2819查詢最新優惠。
            </div>
          </Collapse>
        </div>
      </>
    )
  }
}

export default TandC;