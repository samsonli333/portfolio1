import React from 'react'
import { State } from '../App';


const modalSwitch = React.createContext<State>({
    currentPageY: 0, modalOpen: false, isSubmitted: false, setSubmitOn: () => { }, switchOn: () => { }, setContact1: () => { }, setContact2: () => { }, setContact3: () => { },
    setContactMethod: () => { }, contact1Value: "",
    contact2Value: "", contact3Value: "", contactMethod: undefined
});

export default modalSwitch;