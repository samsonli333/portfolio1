import React, { FormEvent } from 'react'
import { modalDelete } from '../photo/photo'
import ModalSwitch from './ModalSwitch'
import { State } from '../App'
import { alert } from '../photo/photo'

type Prop = {
    open: boolean
}

type prop2 = {
    contact1Error: boolean,
    contact2Error: boolean,
    contact3Error: boolean,
    contact4Error: boolean,
    errorNotice: boolean
}

interface State2 {
    [contact1Error:string]: boolean,

    contact2Error: boolean,
    contact3Error: boolean,
    contact4Error: boolean,
    errorNotice: boolean
}

class ModalForm extends React.Component<Prop, State2>{
    public state: State2
    constructor(props: Prop) {
        super(props)
        this.state = {
            contact1Error: false,
            contact2Error: false,
            contact3Error: false,
            contact4Error: false,
            errorNotice: false
        }
    }

    public render() {

        return (
            <ModalSwitch.Consumer>
                {({ contact1Value, contact2Value, contact3Value, contactMethod, switchOn, isSubmitted, setSubmitOn }: State) => {
                    const Submit = (e: FormEvent<HTMLFormElement>) => {
                        e.preventDefault();
                        const isError = () => {
                            const items = [contact1Value, contact2Value, contact3Value, contactMethod] as unknown as keyof State
                            for (let i=0; i < items.length;i++){
                                if(!items[i]){
                                    this.setState(state => ({...state,[`contact${i+1}Error`]:true,errorNotice:true}))
                                }else{
                                    this.setState(state => ({...state,[`contact${i+1}Error`]:false,errorNotice:false}))
                                }
                            }
                        }

                        const sequenceCall = () => {

                            const currentURL = decodeURI(window.location.href);
                            let keyword = currentURL.substring(currentURL.search("promotion=") + 10);
                            if (currentURL.search("promotion=") > 0) {
                                if (keyword.search("&") > 0) {
                                     keyword = keyword.substring(0, keyword.search("&"))
                                }
                            } else {
                                keyword = "(沒有)";
                            }

                            const greeting = "你好,想查詢優惠詳情,\n "
                            const message1 = "姓名 : " + contact1Value + "," 
                            const message2 = "國家編號 : " + contact2Value + "," 
                            const message3 = "電話號碼 : " + contact3Value + "," 
                            const message4 = "關鍵字: " + keyword
                            const allMessage = encodeURI(greeting + message1 + "\n" + message2 + "\n" + message3 + "\n" + message4);

                            const randomNum = Math.floor(Math.random() * 3) + 1
                            
                            switch (randomNum) {
                                case 1:
                                    window.open('https://wa.me/85262341535?text=' + allMessage, '_blank');
                                    break;
                                case 2:
                                    window.open('https://wa.me/85297804905?text=' + allMessage, '_blank');
                                    break;
                                case 3:
                                    window.open('https://wa.me/85270721837?text=' + allMessage, '_blank');
                            }

                        }

                        const toCall = () => {
                            if (contactMethod === "2") sequenceCall();
                            setSubmitOn();
                        }

                        (!contact1Value || !contact2Value || !contact3Value || !contactMethod) ? isError() : toCall();

                    }

                    return (
                        <div>
                            {this.props.open && (
                                <div className="modalForm-container" >
                                    <div className="form-outermost">
                                        <img alt="#" className="modal-delete-icon" src={modalDelete} onClick={() => switchOn()}></img>
                                        <div className="form-outer">
                                            <div className="form-outer-title" >馬上訂閱！</div>
                                            <form className="form" onSubmit={Submit}>
                                                <div className="form-title">請填寫下面的表格，<br />我們會跟進您的詢問。</div>
                                                {!contactMethod ? <Details contact1Error={this.state.contact1Error} contact2Error={this.state.contact2Error}
                                                    contact3Error={this.state.contact3Error} contact4Error={this.state.contact4Error} errorNotice={this.state.errorNotice} />
                                                    : (contactMethod === "1" && isSubmitted) ? <CallNow /> : isSubmitted ? <WhatsappNow /> :
                                                        <Details contact1Error={this.state.contact1Error} contact2Error={this.state.contact2Error}
                                                            contact3Error={this.state.contact3Error} contact4Error={this.state.contact4Error} errorNotice={this.state.errorNotice} />}
                                            </form>
                                        </div>
                                    </div>
                                </div>)}
                        </div>
                    )
                }}
            </ModalSwitch.Consumer>
        )
    }
}

const CallNow = () => (
    <ModalSwitch.Consumer>
        {({ switchOn }: State) => (
            <>
                <div className="callNow">你的資料已成功提交，<br />
                    於3天工作天內將會有專人與你聯絡。</div>
                <input type="button" value="關閉" onClick={() => switchOn()} />
            </>
        )}
    </ModalSwitch.Consumer>)


const WhatsappNow = () => (
    <ModalSwitch.Consumer>
        {({ contact1Value,contact2Value,contact3Value}: State) => {

const sequenceCall = () => {

    const currentURL = decodeURI(window.location.href);
    let keyword = currentURL.substring(currentURL.search("promotion=") + 10);
    if (currentURL.search("promotion=") > 0) {
        if (keyword.search("&") > 0) {
             keyword = keyword.substring(0, keyword.search("&"))
        }
    } else {
        keyword = "(沒有)";
    }

    const greeting = "你好,想查詢優惠詳情,\n "
    const message1 = "姓名 : " + contact1Value + "," 
    const message2 = "國家編號 : " + contact2Value + "," 
    const message3 = "電話號碼 : " + contact3Value + "," 
    const message4 = "關鍵字: " + keyword
    const allMessage = encodeURI(greeting + message1 + "\n" + message2 + "\n" + message3 + "\n" + message4);

    const randomNum = Math.floor(Math.random() * 3) + 1
    
    switch (randomNum) {
        case 1:
            window.open('https://wa.me/85262341535?text=' + allMessage, '_blank');
            break;
        case 2:
            window.open('https://wa.me/85297804905?text=' + allMessage, '_blank');
            break;
        case 3:
            window.open('https://wa.me/85270721837?text=' + allMessage, '_blank');
    }

}

return(
            <>
                <div className="whatsappNow">你的資料已成功提交，<br />立即點擊以下WhatsApp連結與我們的客服對話。</div>
                <input type="button" value="連結對話" onClick={() => sequenceCall()} />
            </>
      )
        }}
    </ModalSwitch.Consumer>)


const Details = (prop: prop2) => (
        <ModalSwitch.Consumer>
            {({ setContact1, setContact2, setContact3, setContactMethod }: State) => (
                    <>
                        <div className="details">
                            <input className={prop.contact1Error ? "errorHightlight" : ""} type="text" placeholder="你的名字" onChange={e => setContact1(e.target.value)} />
                            <div className="form-Contact">
                                <select className={`${prop.contact2Error && "errorHightlight"} form-Contact1`} value={undefined} onChange={e => setContact2(e.target.value)}>
                                    <option disabled hidden value={undefined}>+852</option>
                                    <option value="852">+852</option>
                                </select>
                                <input className={`${prop.contact3Error && "errorHightlight"} form-Contact2`} type="text" placeholder="你的電話號碼" onChange={e => setContact3(e.target.value)} />
                            </div>
                            <select className={prop.contact4Error ? "errorHightlight" : ""} value={undefined} onChange={e => setContactMethod(e.target.value)}>
                                <option hidden value={undefined}>聯絡方法</option>
                                <option value="1">電話聯絡</option>
                                <option value="2">Whatsapp</option>
                            </select>
                            {prop.errorNotice && <div className="errorNotice">
                                <div className="form-contact4"><img alt="#" src={alert} /><div>請填寫資料並確保資料為正確</div></div>
                            </div>}
                        </div>
                        <input type="submit" value="提交" />
                    </>
                )
            }
        </ModalSwitch.Consumer>
    )


export default ModalForm;