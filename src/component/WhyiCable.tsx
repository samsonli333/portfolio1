import React from 'react'
import { whyiFeature, whyIcon1, whyIcon2, whyIcon3, whyIcon4 } from '../photo/photo'
import { InlineShareButtons } from 'sharethis-reactjs'

class WhyiCable extends React.Component {

  public render() {
    return (
      <> <div className="why-feature"><img alt="#" className="why-feature-img" src={whyiFeature}></img></div>
        <div className='why-explanation'>
          <div className="why-explanation-detail">
            <img alt="#" className="why-icon" src={whyIcon1}></img>
            <div className="why-detail-title">豁免安裝費</div>
            <div className="why-detail">豁免家居寬頻基本安裝費，原價港幣$600</div>
          </div>
          <div className="why-explanation-detail">
            <img alt="#" className="why-icon" src={whyIcon2}></img>
            <div className="why-detail-title">真正光纖入屋</div>
            <div className="why-detail">i-CABLE光纖寛頻服務，廣泛覆蓋港九新界。憑藉技術升級，光纖頻寬上限已提升至下載及上載1Gbps，穩定流暢</div>
          </div>
          <div className="why-explanation-detail">
            <img alt="#" className="why-icon" src={whyIcon3}></img>
            <div className="why-detail-title">穩定寬頻服務保證</div>
            <div className="why-detail">設有服務保證計劃，保障您的寬頻上網體驗 (適用於指定光纖服務計劃)</div>
          </div>
          <div className="why-explanation-detail">
            <img alt="#" className="why-icon" src={whyIcon4}></img>
            <div className="why-detail-title">全方位專業支援</div>
            <div className="why-detail">專業的銷售、安裝、技術支援及客戶服務團隊，全方位為您提供優質服務。隨時與我們聯絡，為您提供及時協助。</div>
          </div>
        </div>
        <div className="share">
          <div className="share-friend">與朋友分享</div>
          {/* <div className="sharethis-inline-share-buttons"></div> */}
          <InlineShareButtons
              config={{
                alignment: 'center',  // alignment of buttons (left, center, right)
                color: 'social',      // set the color of buttons (social, white)
                enabled: true,        // show/hide buttons (true, false)
                font_size: 16,        // font size for the buttons
                labels: null,        // button labels (cta, counts, null)
                language: 'en',       // which language to use (see LANGUAGES)
                networks: [           // which networks to include (see SHARING NETWORKS)
                  'whatsapp',
                  'twitter',
                  'facebook',
                  'linkedin',
                ],
                padding: 10,          // padding within buttons (INTEGER)
                radius: 100,            // the corner radius on each button (INTEGER)
                show_total: true,
                size: 40,             // the size of each button (INTEGER)

                // OPTIONAL PARAMETERS
                url: 'https://www.2power-hk.com', // (defaults to current url)
                image: 'https://bit.ly/2CMhCMC',  // (defaults to og:image or twitter:image)
                description: 'custom text',       // (defaults to og:description or twitter:description)
                title: 'custom title',            // (defaults to og:title or twitter:title)
                message: 'custom email text',     // (only for email sharing)
                subject: 'custom email subject',  // (only for email sharing)
                username: 'custom twitter handle' // (only for twitter sharing)
              }}
              ></InlineShareButtons>
        </div>
      </>
    )
  }
}

export default WhyiCable;