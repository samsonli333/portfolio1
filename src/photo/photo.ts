import Logo from './download/Logo.svg'
import whatsappIcon from './download/Whatsapp-Icon.png'
import bgPicDesktop from './i-CABLE Broadband_1000M_Hero Image_Desktop_778px.png'
import whyiFeature from './i-CABLE Broadband_feature_WhyiCableTitle_430px.png'
import whyIcon1 from './i-CABLE Broadband_feature_icon01_100px.png'
import whyIcon2 from './i-CABLE Broadband_feature_icon02_100px.png'
import whyIcon3 from './i-CABLE Broadband_feature_icon03_100px.png'
import whyIcon4 from './i-CABLE Broadband_feature_icon04_100px.png'
import vectorWhats from './download/Vector (2).png'
import vectorTel from './download/Vector.png'
import vectorFace from './download/Vector (4).png'
import vectorEmail from './download/Vector (3).png'
import logoWhite from './download/logowhite.svg'
import arrowTandC from './download/chevron-down.svg'
import bgPicMobile from './i-CABLE Broadband_1000M_Hero Image_mobile_375px.png'
import modalDelete from './download/delete.png'
import alert from './download/alert-circle.svg'


export {Logo, logoWhite,whatsappIcon,bgPicDesktop, bgPicMobile, whyiFeature, 
    whyIcon1, whyIcon2,whyIcon3,whyIcon4,vectorWhats,vectorTel,vectorFace,vectorEmail,arrowTandC,modalDelete,alert}