import React from 'react'
import ModalForm from './component/ModalForm';
import TandC from './component/TandC';
import WhyiCable from './component/WhyiCable';
import { Header } from './function/function'
import { logoWhite, vectorTel, vectorEmail, Logo } from './photo/photo'
import ModalSwitch from './component/ModalSwitch'
import TopNav from './component/TopNav';
import BottomNav from './component/BottomNav';



export interface State {
  currentPageY: number,
  modalOpen: boolean,
  isSubmitted: boolean,
  setSubmitOn: () => void,
  switchOn: () => void,
  setContact1: (x: string) => void,
  setContact2: (x: string) => void,
  setContact3: (x: string) => void,
  setContactMethod: (x: string) => void,
  contact1Value: string,
  contact2Value: string | undefined,
  contact3Value: string,
  contactMethod: string | undefined,
}

class App extends React.Component<{}, State>{
  public state: State
  constructor(props: {}) {
    super(props)
    this.state = {
      currentPageY: window.pageYOffset,
      modalOpen: false,
      isSubmitted: false,
      setSubmitOn: this.setSubmitOn,
      switchOn: this.setOpen,
      setContact1: this.setContact1Value,
      setContact2: this.setContact2Value,
      setContact3: this.setContact3Value,
      setContactMethod: this.setContactMethod,
      contact1Value: "",
      contact2Value: undefined,
      contact3Value: "",
      contactMethod: undefined
    }
  }

  public componentDidMount() {
    const header: Header = new Header();
    header.location();
    window.addEventListener('scroll', () => {
      if (Math.abs(this.state.currentPageY - window.pageYOffset) >= 120) {
        if (window.pageYOffset > this.state.currentPageY) {
          header.Hidden();
        } else {
          header.Show();
        }
        this.setState({ currentPageY: window.pageYOffset })
      }
    })
  }

  setOpen = () => {
    this.setState(state => ({ modalOpen: !state.modalOpen }))
  }

  setContact1Value = (x: string) => {
    this.setState({ contact1Value: x })
  }

  setContact2Value = (x: string) => {
    this.setState({ contact2Value: x })
  }

  setContact3Value = (x: string) => {
    this.setState({ contact3Value: x })
  }

  setContactMethod = (x: string) => {
    this.setState({ contactMethod: x })
  }

  setSubmitOn = () => {
    this.setState({ isSubmitted: true })
  }


  public render() {
    return (
      <ModalSwitch.Provider value={this.state} >
        <ModalSwitch.Consumer>
          {({ modalOpen }: State) => (
            <div className="mainwrapper">
              <header className="header">
                <div className="brandLine"></div>
                <TopNav />
              </header>
              <section className="heroSection"></section>
              <section className="whyISection"><WhyiCable /></section>
              <section className="t-and-c-section"><TandC /></section>
              <BottomNav />
              <footer className="footer-page">
                <img className="footer-logo" alt="#" src={Logo}></img>
                <div className="footer-contact">
                  <div className="footer-contact1"><img className="footer-img1" alt="#" src={vectorTel}></img><a href="tel:62341535">183 2832</a></div>
                  <div className="footer-contact2"><img className="footer-img2" alt="#" src={vectorEmail}></img><a href="mailto:sales@i-icable.com">sales@i-cable.com</a></div>
                </div>
                <div className="footer-statement">個人信息收集聲明<br />© 2022 Hong Kong Cable Television Limited Disclaimer & Copyright(All Rights Reserved)</div>
              </footer>
              <ModalForm open={modalOpen} />
            </div>)}
        </ModalSwitch.Consumer>
      </ModalSwitch.Provider >
    )
  }
}

export default App;
