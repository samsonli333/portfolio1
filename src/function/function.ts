export class arrow {

    public up(): void {
        document.querySelector('.arrow')?.classList.add('arrow-up')
    }

    public down(): void {
        document.querySelector('.arrow')?.classList.remove('arrow-up')
    }
}


export class Header {
    public header: HTMLElement = document.getElementsByTagName('header')[0]

    public location():void{
        this.header.style.position = "sticky"
        this.header.style.top = "0%"
        this.header.style.left = "0%"
        this.header.style.right = "0%"
        this.header.style.bottom = "auto";
    }

    public Hidden(): void {   
        this.header.style.transition = "transform 1s"
        this.header.style.transform = "translateY(-100%)";
    }
    
    public Show(): void {
        this.header.style.transform = "translateY(0%)"
    }

}


export class whatsapp {

    public static genericCall(): void {
        window.open('https://wa.me/85262341535?text="hello"', "_blank")
    }
    
}